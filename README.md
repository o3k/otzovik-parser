# Парсер otzovik.com


## To-do list

### Синхронизация с хостингом
Скрипт синхронизации с хостингом, куда выгружаются картинки


## Структура проекта

### Ежедневное задание
* grab-sitemap.sh - скачивает sitemap на текущую дату
* scripts/start.php - обходит список new.txt


#### grab-sitemap.sh
Скачивает и распаковывает sitemap.xml-файлы.
Всё остальное мусор, **включая папку /content/**!

#### grab-wget.sh (устарело!)
обходит список sitemap/$DATA/new.txt и сохраняет данные в content/

#### scripts/start.php
Обходит список new.txt. Скачивает страницу, вытаскивая нужный текст по селекторам. Скачивает картинки, обрезая логотип. Решает капчу, когда необходимо. Ложит всё в БД. Текущая БД не конечная: сайты её не используют, чтобы отрисовать её содержимое.

#### scripts/removed_links_db_import.php YY-MM-DD
Импортирует ссылки из файла removed.txt в БД. Запускается из `grab-sitemap.sh`. Аргумент обязателен!
