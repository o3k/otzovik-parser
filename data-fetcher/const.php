<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 23.11.2017
 * Time: 23:18
 */

define('SITEMAP_FOLDER', __DIR__ . '/../sitemap/');
define('IMG_FOLDER', __DIR__ . '/../data/img/');
define('LAST_POSITION', __DIR__ . '/app/last_position.json');
define('ANTI_CAPTCHA_DIR', __DIR__ . '/app/lib/anticaptcha');

