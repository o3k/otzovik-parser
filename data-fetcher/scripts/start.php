<?php
// Vendor, польз. классы, настрйки, константы
require __DIR__ . '/../bootstrap.php';

use GuzzleHttp\Client as Client;
use App\Helper as H;
use App\UrlList;
use App\Crawler;
use App\Classes\DB;
use App\Classes\Connection;

// @todo: принимает аргумент как ссылку. если он есть, то не загружать ссылки из файла new.txt
//var_dump($argv[1]);
//die();

// Извлекает список ссылок
// Если загрузка была не закончена, продолжает её с того же места
// ''|test|local_test используются для отладки
$links = new UrlList();
//H::log(get_object_vars($links), 'Св-ва объекта после его создания');
$links = $links->getLinks();
//H::log($links, 'Итоговый массив ссылок для загрузки');


$client = new Client([
//	'base_uri' => $url,
	'timeout' => 30.0,
	'headers' => [
		'User-Agent' => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36",
	],
	'cookies' => true, // работает глобально и должно быть по-умолчанию
	'http_errors' => FALSE,
]);


// Request headers для страницы и картинок
require __DIR__ . '/../app/headers.php'; // $request_headers

$db = new DB;
$counter = 0;
$start_time = time();

foreach ($links as $url) {
	H::log($url, 'Current URL');

	$response = Connection::makeSafe($client, 'GET', $url, $request_headers->page);

	// Если вернулась страница
	if ( $response->getStatusCode() === 200 ) {

		// собирает нужные поля, сохраняя их в свойства объекта
		$content = new Crawler($response->getBody()->getContents(), $url);
//		H::log(get_object_vars($content), 'Результат обработки страницы');

		// Сохранить текущую позицию в last_position.json
		UrlList::saveCurrentPosition($url);

		// отправляет подготовленные данные в БД
		$db->insertReview($content);
		echo 'Saved' . PHP_EOL;
		echo date('H:i:s', time() - $start_time) . PHP_EOL;
		unset($content);

		if ( $response->getHeader('set-cookie') ) {
			H::log($response->getHeader('set-cookie')[0], 'Set-cookie value');
		}

	// Если вернулась капча
	} else if ( $response->getStatusCode() === 503 ) {
		// Показывает куки, если они есть (иначе пустой массив)
//		$cookie = $client->getConfig('cookies')->toArray();
//		H::log($cookie, 'Cookies');

		H::log($counter, 'Links parsed before the captcha');
		$end_time = round( (time() - $start_time) / 60 );
		H::log($end_time . ' minutes', 'Time without a captcha');
		H::log(date('H:i:s', time()), 'The captcha was triggered in');

		$captcha = new \App\Classes\AntiCaptcha();
		$html = $response->getBody()->getContents();
//		var_dump($html);
		// извлекает ссылку на изображение с капчей
		$img_src = $captcha->getCaptchaUrl($html, $url);

		// загружает картинку с капчей
		$img_response = Connection::makeSafe($client,'GET', $img_src, $request_headers->img);
//		$img_response = $client->request('GET', $img_src, $request_headers->img);

		$base64 = $captcha->getBase64($img_response);
		file_put_contents(__DIR__ . '/test.txt',
			'data:' . $img_response->getHeader("Content-Type")[0] .
			';base64,' . $base64
		);

		$answer = $captcha->resolve($base64);
		H::log($answer, 'The captcha answer is');


		// отправляет капчу, вызывает текущую страницу повторно, когда разгадал капчу
		var_dump($captcha->prepareRequset());
		$response = Connection::makeSafe($client, 'POST', $url, [
			'headers' => $request_headers->page['headers'],
			'form_params' => $captcha->prepareRequset(),
		]);
//		$response = $client->request('POST', $url, [
//			'headers' => $request_headers->page['headers'],
//			'form_params' => $captcha->prepareRequset(),
//		]);

		if ($response->getStatusCode() === 200) {
			// собирает нужные поля, сохраняя их в свойства объекта
			$content = new Crawler($response->getBody()->getContents(), $url);

			// Сохранить текущую позицию в last_position.json
			UrlList::saveCurrentPosition($url);

			// отправляет подготовленные данные в БД
			$db->insertReview($content);

			unset($content);
		} else {
			H::log('Possible 404 after captcha', 'After captcha answer');
			// @todo: обработать 404, некоторые из них на самом деле не 404 (расширить Crawler, реализовав проверку)
			//			var_dump($response->getBody()->getContents());
//			die();
		}




	} else if ( $response->getStatusCode() === 404 ) {
		echo 'The link was ignored. Status code: ' . $response->getStatusCode() . PHP_EOL;
		// @todo: обработать 404, некоторые из них на самом деле не 404 (расширить Crawler, реализовав проверку)

	}

//	break; // for dev
	$counter++;
	H::sleep(9);
}

