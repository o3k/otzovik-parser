<?php
/********************************************
 * Добавляет ссылки из файла removed.txt в БД
 ********************************************/

require __DIR__ . '/../vendor/autoload.php';

use App\Classes\DB;

$date = $argv[1];
if (!$date) die( 'removed_links_db_import.php:: no argument was given!' );

// подготовка списка ссылок
$source = file_get_contents(__DIR__ . "/../../sitemap/$date/removed.txt");
$array_list = explode("\n", $source);
unset($array_list[count($array_list) - 1]);

// добавить в БД
$db = new DB;
$db->addRemovedLinks($array_list);