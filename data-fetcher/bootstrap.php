<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 23.11.2017
 * Time: 19:32
 */

require __DIR__ . '/vendor/autoload.php';

// Настройки и константы
require __DIR__ . '/const.php';

// Статические методы-помошники
require __DIR__ . '/app/Helper.php';

// Ссылки для загрузки
require __DIR__ . '/app/UrlList.php';

// Парсер по селекторам
require __DIR__ . '/app/Crawler.php';

// Библиотека Антикапчи
require __DIR__ . '/lib/anticaptcha/anticaptcha.php';
require __DIR__ . '/lib/anticaptcha/imagetotext.php';
