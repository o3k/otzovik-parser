<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 26.11.2017
 * Time: 19:59
 */

$request_headers = new stdClass();

//GET /review_526010.html HTTP/1.1
//Host: otzovik.com
//Connection: keep-alive
//Pragma: no-cache
//Cache-Control: no-cache
//User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36
//Upgrade-Insecure-Requests: 1
//Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
//Referer: http://otzovik.com/reviews/epilyator_panasonic_es-2023_a/
//Accept-Encoding: gzip, deflate
//Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7
//Cookie: refreg=http%3A%2F%2Fotzovik.com%2Freview_5650322.html; ROBINBOBIN=u1fedvnenm8s142nktcebdk0c4

$request_headers->page = [ 'headers' =>
	[
		'Pragma' => 'no-cache',
		'Cache-Control' => 'no-cache',
		'Upgrade-Insecure-Requests' => '1',
		'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
		'Accept-Encoding' => 'gzip, deflate',
		'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
	]
];

//GET /2013/09/05/526010/img/74060378.jpg HTTP/1.1
//Host: i.otzovik.com
//Connection: keep-alive
//Pragma: no-cache
//Cache-Control: no-cache
//User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36
//Accept: image/webp,image/apng,image/*,*/*;q=0.8
//Referer: http://otzovik.com/review_526010.html
//Accept-Encoding: gzip, deflate
//Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7

$request_headers->img = [ 'headers' =>
	[
		'Pragma' => 'no-cache',
		'Cache-Control' => 'no-cache',
		'Accept' => 'image/webp,image/apng,image/*,*/*;q=0.8',
		'Accept-Encoding' => 'gzip, deflate',
		'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
	]
];
