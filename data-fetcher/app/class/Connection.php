<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 13.01.2018
 * Time: 12:44
 */

namespace App\Classes;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;


class Connection {

//	function __construct($client, $method, $url, $options) {
//
//	}

	/**
	 * Если нет соединения с сайтом, повторит попытку (вместо Unhandled
	 * Exception)
	 *
	 * @param \GuzzleHttp\Client $client
	 * @param $method string GET|POST
	 * @param $url string полный URL
	 * @param $options array настройки соединения
	 *
	 * @return mixed|\Psr\Http\Message\ResponseInterface
	 */
	static function makeSafe(Client $client, $method, $url, $options = []) {

		$flag = null;
		while (!$flag) {
			try {
				$response = $client->request($method, $url, $options);
				$flag = TRUE;

			} catch ( ConnectException $e ) {
				echo 'no connection at the moment. waiting for 5 second...' . PHP_EOL;
				echo $e->getCode() . ' ' .
					 $e->getMessage()  . ' ' .
					 $e->getLine()  . ' ' .
					 $e->getFile() . PHP_EOL;

				sleep(5);
				$flag = FALSE;
			}
		}
		return $response;
	}


	static function ping($domain) {

		$response = null;
		system("ping -c 1 $domain > /dev/null", $response);
		if ($response === 0) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

}