<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 10.01.2018
 * Time: 9:06
 */

namespace App\Classes;
use Symfony\Component\DomCrawler\Crawler;

class AntiCaptcha {

	private $api;

	public $parsedUrl;

	public $answer;

	function __construct() {
		$this->api = new \ImageToText();
		$this->api->setVerboseMode(true);
		$this->api->setKey('926d0934bfdad0b57681635b0d1a9089');
	}

	function getCaptchaUrl($page, $url) {

		// извлекает ссылку на изображение captcha
		$page = new Crawler($page);
		$image_src = $page->filter('table img')->attr('src');

		$this->parsedUrl = parse_url($url);
//		var_dump($this->parsedUrl['path']);
//		die();
		$base_path = $this->parsedUrl['scheme'] . '://' . $this->parsedUrl['host'];

		$img_url = $base_path . $image_src;

		return $img_url;
	}

	function getBase64($response) {

		$mime_type = $response->getHeader("Content-Type")[0];
		$size = $response->getBody()->getSize();
		$raw_img = $response->getBody()->read($size);
//		return 'data:' . $mime_type . ';base64,' . base64_encode($raw_img);
		return base64_encode($raw_img);

	}


	/**
	 * @param $base64 string изображение капчи
	 *
	 * @return string|bool результат разгаданной капчи
	 */
	function resolve($base64) {

		$this->api->setBase64($base64);

		if (!$this->api->createTask()) {
			$this->api->debout("API v2 send failed - ".$this->api->getErrorMessage(), "red");
			return false;
		}

		$taskId = $this->api->getTaskId();


		if (!$this->api->waitForResult()) {
			$this->api->debout("could not solve captcha", "red");
			$this->api->debout($this->api->getErrorMessage());
		} else {
			echo "\nhash result: ".$this->api->getTaskSolution()."\n\n";
		}

		$this->answer = $this->api->getTaskSolution();
		return $this->answer;
	}

	function prepareRequset() {
		$post = [
			'captcha_url' => $this->parsedUrl['path'],
			'keystring' => $this->answer,
			'action_captcha_ban' => ' Я не робот! ',
		];
		return $post;
	}

}
