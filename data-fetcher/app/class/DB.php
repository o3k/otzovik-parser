<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 08.01.2018
 * Time: 9:06
 */

namespace App\Classes;
use Illuminate\Database\Capsule\Manager;
use App\Crawler;
use App\Helper as H;



class DB {
	private $settings = [
		'driver' => 'mysql',
		'host' => 'localhost',
		'database' => '4otziv',
		'username' => '4otziv',
		'password' => 'lqze352edkjd64K6HSpQ',
		'charset' => 'utf8mb4',
		'collation' => 'utf8mb4_general_ci',
		'prefix' => '',
	];

	/**
	 * Готовое к использованию соединение с базой данных
	 *
	 * @var \Illuminate\Database\Capsule\Manager
	 */
	protected $db;

	function __construct() {
		$capsule = new Manager();
		$capsule->addConnection($this->settings);
		$capsule->setAsGlobal();
		$capsule->bootEloquent();
		$this->db = $capsule;
	}

	/**
	 * Отправить подготовленные данные в БД
	 *
	 * @param \App\Crawler $content
	 */
	function insertReview(Crawler $content) {

		if ($content->recommendation === 'ДА') {
			$content->recommendation = 1;
		} else {
			$content->recommendation = 0;
		}

		$data = [
			'url' => $content->url,
			'breadcrumbs' => json_encode($content->breadcrumbs, JSON_UNESCAPED_UNICODE),
			'product_title' => $content->productTitle,
			'review_reputation' => $content->reviewReputation,
			'review_title' => $content->reviewTitle,
			'pros' => $content->prosAndCons['pros'],
			'cons' => $content->prosAndCons['cons'],
			'description' => $content->description,
			'summary' => $content->summary,
			'is_recommended' => $content->recommendation,
			'rating' => $content->rating,
			'created_at' => date('Y-m-d H:i:s'),
		];

		$this->db->table('review')->insert($data);
	}

	/**
	 * Добавляет только новые ссылки в таблицу removed
	 *
	 * @param $url_list array Неассоциативный массив ссылок
	 */
	function addRemovedLinks($url_list) {
		foreach ($url_list as $url) {
			$r = $this->db->table('removed')->get(['url'])->where('url', '=', $url)->all();
			if (!$r) {
				$this->db->table('removed')->insert(['url' => $url]);
				echo "$url was added because this is a new item" . PHP_EOL;
			} else {
				echo "$url is exists already" . PHP_EOL;
			}
		}
	}

}