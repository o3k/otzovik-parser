<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 05.12.2017
 * Time: 16:24
 */
namespace App;
use \App\Helper as Helper;


class UrlList {
	public $test_links = [
		'https://otzovik.com/review_1850914.html',
		'http://otzovik.com/review_3258240.html',
		'http://otzovik.com/review_5721036.html',
		'http://otzovik.com/review_3614710.html',
		'http://otzovik.com/review_3614711.html',
		'http://otzovik.com/review_3614714.html',
		'http://otzovik.com/review_3614715.html',
		'http://otzovik.com/review_3614716.html',
		'http://otzovik.com/review_3614717.html',
		'http://otzovik.com/review_4264183.html',
		'http://otzovik.com/review_4264184.html',
		'http://otzovik.com/review_4264185.html',
		'http://otzovik.com/review_4264186.html',
		'http://otzovik.com/review_4264187.html',
		'http://otzovik.com/review_4264188.html',
		'http://otzovik.com/review_4264189.html',
		'http://otzovik.com/review_4264190.html',
		'http://otzovik.com/review_4264191.html',
		'http://otzovik.com/review_4264194.html',
		'http://otzovik.com/review_4264195.html',
	];

	public $linksMode;

	public $links;

	public $lastPosition;

	public $lastPositionKey;

	public $newPos;

	/**
	 * Извлекает спискок ссылок, преобразуя его в индексированный массив.
	 *
	 * @param string $mode test|local_test используются для отладки
	 *
	 */
	function __construct($mode = '') {
		if (!$mode) {

			$links = new \stdClass();

			$links->ls = trim( shell_exec('ls '. SITEMAP_FOLDER), "\n" );
			$links->ls = explode("\n", $links->ls );
			//Helper::log($links->ls, 'sitemap folders list as array');

			// Путь к папкам, которые понадобятся
			$links->ls_length = count($links->ls);
			//Helper::log($links->ls_length, 'Sum of array elements');

			$last_folder = SITEMAP_FOLDER . $links->ls[$links->ls_length - 2];
			$curr_folder = SITEMAP_FOLDER . $links->ls[$links->ls_length - 1];
			//Helper::log([$last_folder, $curr_folder], 'Last and Today folders');

			// Извлекает и готовит ссылки
			$links->filePath =  $curr_folder . '/new.txt';
			echo PHP_EOL . "Список ссылок для обхода: " . $links->filePath . PHP_EOL;

			$links->rawList = file_get_contents( $links->filePath );
			$links->rawList = trim($links->rawList, "\n");

			$links->list = explode("\n", $links->rawList);
		}

		$test_local_url = ['http://slim.edu/guzzle-test/review', 'http://slim.edu/guzzle-test/503'];

		switch ($mode) {
			case('test'): $result = $this->test_links;
				echo PHP_EOL . 'TEST list was return' . PHP_EOL;
				break;
			case('local_test'): $result = $test_local_url;
				echo PHP_EOL . 'LOCAL TEST list was return' . PHP_EOL;
				break;
			case(''): $result = $links->list;
				echo PHP_EOL . 'NEW.TXT list was return' . PHP_EOL;
				break;
			default: $result = NULL;
		}

		$this->linksMode = $mode; // нужно чтобы пропустить это при отладке
		$this->links = $result;

	}


	/**
	 * Возвращает последний обработанный url, если он есть
	 */
	public function getLastPosition() {
		$result = NULL;

		if (file_exists(LAST_POSITION)) {
			Helper::log("1. last_position.json is exists");
			echo LAST_POSITION;

			$last_position = new \stdClass();
			$last_position->input_file = file_get_contents(LAST_POSITION);
			$last_position->array = json_decode($last_position->input_file, TRUE);

			// an oche silni validation
			if ($last_position->array &&
				isset($last_position->array[date("Y-m-d")]) &&
				!empty($last_position->array[date("Y-m-d")])
			) {
				$last_position->today_data = $last_position->array[(string) date("Y-m-d")];
				Helper::log($last_position->today_data, 'The last pos is');
				$result = $last_position->today_data;

			} else {
				Helper::log('2. the array is empty OR no data for today');
			}
		}

		$this->lastPosition = $result;
	}

	/**
	 * Возвращает новый массив с учетом последней позиции
	 *
	 */
	public function setNewPos() {
		// пропустить, если тестовый режим
		if ( !$this->linksMode && $this->lastPosition) {
			$this->lastPositionKey = (int)array_search($this->lastPosition, $this->links, true);
			$this->newPos = array_slice($this->links, $this->lastPositionKey + 1, null, true);
		}
	}

	/**
	 * Обработка ссылок
	 */
	public function getLinks() {

		self::getLastPosition();
		self::setNewPos();

		if ($this->newPos) {
			$links = $this->newPos;
		} else {
			$links = $this->links;
		}
		return $links;
	}

	public static function saveCurrentPosition($url) {

		$data = [date('Y-m-d') => $url];
		$data = json_encode($data, \JSON_PRETTY_PRINT);
		file_put_contents(LAST_POSITION, $data);
	}
}