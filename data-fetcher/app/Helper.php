<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 23.11.2017
 * Time: 17:31
 */

namespace App;


class Helper
{
	function __construct() {
		echo __METHOD__;
	}

	/**
	 * Pretty log output
	 *
	 * @param mixed $var
	 * @param string $comment
	 */
	public static function log($var, $comment = null) {
		if ($comment) {
			echo PHP_EOL . gettype($var) . ":\t" . $comment . ':' . PHP_EOL;
		} else {
			echo PHP_EOL . gettype($var) . ":\t" . PHP_EOL;
		}

		if ( gettype($var) === 'string' || gettype($var) === 'integer' ) {
			echo $var . PHP_EOL;
		} else if (gettype($var) === 'boolean') {
			var_dump($var);
		}
		else {
			print_r($var);
		}
	}

	/**
	 * Represent both --wait and --random-wait parameters from wget
	 *
	 * @param int $seconds
	 * @param bool $random_wait
	 */
	public static function sleep($seconds, $random_wait = true) {
		$seconds = (int)$seconds;
		if ($random_wait) {
			$rand = [ round($seconds * 0.7), round($seconds * 1.2) ];
			$rand = rand( $rand[0], $rand[1] );
			$seconds = $rand;
		}
//		self::log($seconds, 'Sleep time');
		sleep($seconds);
	}
	
}