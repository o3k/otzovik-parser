<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 27.11.2017
 * Time: 9:34
 */

namespace App;
use Symfony\Component\DomCrawler\Crawler as C;
use GuzzleHttp\Client;
use App\Classes\Connection;
use claviska\SimpleImage;

/**
 * Class Crawler собирает объект для сохранения данных этого объекта в БД
 *
 * todo: текущий url как свойство
 *
 */
class Crawler {

	protected $crawler;

	public $url;

	public $breadcrumbs;

	public $productTitle;

	public $reviewReputation;

	public $reviewTitle;

	public $prosAndCons;

	public $description;

	public $descImgUrl;

	public $absImgPath;

	public $relImgPath;

	public $imgPathSection;

	public $newAbsImgUrl;

	public $newRelImgUrl;

	public $summary;

	public $recommendation;

	function __construct($page, $page_url) {
		$this->crawler = new C($page);
		$this->url = addslashes($page_url);

		// вызывает все публичные методы класса
		$class = new \ReflectionClass(__CLASS__);
		$methods = $class->getMethods(\ReflectionMethod::IS_PUBLIC);

		foreach ($methods as $method ) {
			if ($method->name === '__construct') continue;
			self::{$method->name}();
		}

	}

	/**
	 * Убирает двойные пробелы внутри строки, Убирает табуляцию и пробелы
	 * у границ строки.
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	protected static function formatStr($str) {
		return trim( preg_replace('/\s+/', ' ', $str) );
	}

	function getBreadcrumbs() {
		$this->breadcrumbs = $this->crawler->filter('.aline')->text();
		$this->breadcrumbs = explode(' › ', $this->breadcrumbs);
	}

	function getProductTitle() {
		$this->productTitle = $this->crawler->filter('.hproduct .h1')->text();
		$this->productTitle = self::formatStr($this->productTitle);
	}

	function getReviewReputation() {
		$this->reviewReputation = $this->crawler->filter('.main_head_user_detail b')->text();
	}

	function getReviewTitle() {
		$this->reviewTitle = $this->crawler->filter('h1.title_new')->text();
		$this->reviewTitle = self::formatStr($this->reviewTitle);

		// Извлекает реальный заголовок из SEO-заголовка
		$title = $this->productTitle;
		$this->reviewTitle = str_replace($title . ' - ', '', $this->reviewTitle);
		$this->reviewTitle = str_replace('Отзыв: ', '', $this->reviewTitle);
		var_dump($title);
		var_dump($this->reviewTitle);

	}

	/**
	 * Извлекает достоинства и недостатки
	 */
	function getProsAndCons() {
		$this->prosAndCons = [];

		$prosArgs = $this->crawler->filter('.main_content .pro')->text();
		$prosArgs = stripslashes( str_replace("Достоинства:", '', $prosArgs) );
		$prosArgs = self::formatStr($prosArgs);
		$this->prosAndCons['pros'] = $prosArgs;

		$consArgs = $this->crawler->filter('.main_content .contra')->text();
		$consArgs = stripslashes( str_replace("Недостатки:", '', $consArgs) );
		$consArgs = self::formatStr($consArgs);
		$this->prosAndCons['cons'] = $consArgs;
		
	}

	/**
	 * Извлекает материал (текст+картинки) отзыва
	 */
	function getRawDescription() {
		$this->description = $this->crawler->filter('.main_content [itemprop="description"]')->html();
		$this->description = self::formatStr($this->description);
	}

	/**
	 * Извлекает URL картинок
	 */
	public function getDescImgUrl() {
		$description = $this->crawler->filter('.main_content [itemprop="description"]');
		$imgLinks = $description->filter('img.bigimg')->each( function ($node, $index) {
			return $node->attr('src');
		} );
		$this->descImgUrl =  $imgLinks;
	}

	/**
	 * Создает (если необходимо) и возвращает путь к папке, куда будут
	 * сохраняться изображения.
	 *
	 * @param string $section reviews|products
	 */
	public function setImgPath($section = 'reviews') {
		$relPath = '/' . $section . '/' . date('Y') . '/' . date('m') . '/' . date('d');
		$absPath = IMG_FOLDER  . $section . '/' . date('Y') . '/' . date('m') . '/' . date('d');

		if ( ! is_dir( $absPath ) ) {
			mkdir( $absPath, 0755, TRUE );
		}

		$this->relImgPath = $relPath;
		$this->absImgPath = $absPath . '/';
		$this->imgPathSection = $section;
	}

	/**
	 * Копирует картинки, обрезая ватермарки
	 */
	public function copyImg() {
//		if ( ! $this->descImgUrl) return NULL;

		$imgList = $this->descImgUrl;
		$path = $this->absImgPath;

		$newAbsPath = [];
		$newRelPath = [];

		$imgEditor = new SimpleImage();
		$client = new Client([
			'timeout' => 30
		]);
		foreach ($imgList as $source) {

			preg_match('/\.\w+$/', $source, $extension);
			$newFilename = str_replace('.', '', uniqid('', true)) . $extension[0];
			$destination = $path . $newFilename;
			$newRelPath[] = $this->relImgPath . '/' . $newFilename;
//			$newAbsPath[] = $destination;

			// MIME-type зависит от расширения файла
			if ( '.jpg' === strtolower($extension[0]) || '.jpeg' === strtolower($extension[0]) ) {
				$mimeType = 'image/jpeg';
			}
			if ( '.png' === strtolower($extension[0]) ) {
				$mimeType = 'image/png';
			}
			if ( '.gif' === strtolower($extension[0]) ) {
				$mimeType = 'image/gif';
			}

			// загжужает изображение через GuzzleClient, преобразует его в base64
			$response = Connection::makeSafe($client, 'GET', $source);
			$uriUmg = 'data:' . $response->getHeader("Content-Type")[0] .
				';base64,' . base64_encode($response->getBody()->read($response->getBody()->getSize()));

			// отрезает логотип и сохраняет изображение
			$theImg = $imgEditor->fromDataUri($uriUmg);
			$width = $theImg->getWidth();
			$height = $theImg->getHeight();
			if ($height < 150) {
				continue;
			}
			$theImg
				->crop(0,0, $width, $height - 45)
				->toFile($destination, $mimeType, 75);

		}

//		$this->newAbsImgUrl = $newAbsPath;
		$this->newRelImgUrl = $newRelPath;

	}

	/**
	 * Меняет URL картинок на локальные. Использует {IMG_HOST} placeholder
	 */
	public function setDescUrl() {

		$description = $this->description;

		foreach ($this->descImgUrl as $index => $oldUrl) {
			$description = str_replace($oldUrl, '{IMG_HOST}' . $this->newRelImgUrl[$index], $description);
		}

		$this->description = $description;
//		echo PHP_EOL;
//		echo '===================================';
//		echo PHP_EOL;
//		print_r($this->description);
	}

	// $('.main_content i.summary').text()
	function getSummary() {
		try {
			$this->summary = $this->crawler->filter('.main_content i.summary')->text();
			$this->summary = self::formatStr($this->summary);

		} catch (\Exception $e) {
			$this->summary = '';
		}
	}

	function getRecommendation() {
		$this->recommendation = $this->crawler->filter('.main_content .recommend_user b')->text();
		$this->recommendation = self::formatStr($this->recommendation);
	}

	// $('.main_content abbr.rating').attr('title')
	function getRating() {
		$this->rating = $this->crawler->filter('.main_content abbr.rating')->attr('title');
	}

}
