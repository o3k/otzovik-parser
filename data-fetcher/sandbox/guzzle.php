<?php
/**
 * Created by PhpStorm.
 * User: jjlen
 * Date: 08.01.2018
 * Time: 5:42
 */

require __DIR__ . '/../vendor/autoload.php';

use GuzzleHttp\Client as Client;

$client = new Client([
	'timeout' => 5.0,
	'headers' => [
		'User-Agent' => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36",
	],
	'cookies' => true, // работает глобально и должно быть по-умолчанию
	'http_errors' => FALSE,
]);
