#!/bin/bash

DATE=`date '+%y-%m-%d'`
ABS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ABS_DIR=${ABS_DIR}/sitemap/
DIR=$ABS_DIR$DATE

echo $DIR

cd ${ABS_DIR}/content/ # @todo: добавить BASH_SOURCE

wget -k -x -p -nc --wait=2 --random-wait --span-hosts \
  --user-agent="Mozilla/5.0 (Linux; Android 6.0; M5c Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.84 Mobile Safari/537.36" \
  -R "*.gif","*.js","*.css","*.txt" -A "*.html","*.png","*.jpg","*.jpeg" -X /static \
  --header="accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,image/*,*/*;q=0.8" \
  --header="accept-encoding: identity" --header="accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7" \
  --rejected-log=$DIR/err.log \
  -e robots=off -i $DIR/new.txt
#  --header="Cookie: ROBINBOBIN=47n3nurb333pposa8506a1c7f7; refreg=" \

