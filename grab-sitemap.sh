#!/bin/bash

SITEMAP=$1
DATE=`date '+%y-%m-%d'`

ABS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ABS_DIR=${ABS_DIR}/sitemap/
DIR=$ABS_DIR$DATE

# Если папка уже есть, то выйти
if [ -d $DIR  ]; then
	echo "Dir $DIR/ is exists already. Nothing to do. Bye!"
	exit 1
fi

mkdir $DIR
echo "$DIR has been created..."
cd $DIR
echo "Location has been changed to $DIR..."

# Если не указан URL, то использовать этот
if [ "$SITEMAP" = "" ]; then
	SITEMAP="http://otzovik.com/sitemap.xml"
fi
echo "$SITEMAP has been chosen..."

XML=`wget -O - $SITEMAP`
URLS=`echo $XML | egrep -o "<loc>[^<>]*</loc>" | sed -e 's:</*loc>::g'`
echo $URLS | tr ' ' '\n' | wget -i - --wait=1 --random-wait -nv 


# Выковыривает нужные ссылки
# --------------------------

gunzip $DIR/*

cat $DIR/* > $DIR/all.temp
rm $DIR/*.xml
egrep -o "<loc>.+/review_.+</loc>" $DIR/all.temp | sed 's/<[^>]*>//g' > $DIR/links.txt
rm $DIR/all.temp


# Сравнение последней и текущей папок
# -----------------------------------

# предпоследняя папка
LAST_DIR=`ls -d ${ABS_DIR}*/ | cat | tail -n -2 | head -n +1`

# Если предпоследняя и текущая папка одно и тоже, то выходим
if [ "$LAST_DIR" = "$DIR/" ]; then
  echo "The Lastest Dir and Current Dir are matched. Nothing to do"
  exit 0
fi

diff -u ${LAST_DIR}/links.txt ${DIR}/links.txt | egrep "^-" | sed 's/^-//' | tail -n +2 > $DIR/removed.txt
diff -u ${LAST_DIR}/links.txt ${DIR}/links.txt | egrep "^\+" | sed 's/^\+//' | tail -n +2 > $DIR/new.txt
zip ${LAST_DIR}/links ${LAST_DIR}/links.txt
rm ${LAST_DIR}/links.txt

#  Добавляет removed текущего дня в БД
# php ${ABS_DIR}../data-fetcher/scripts/removed_links_db_import.php $DATE

# @todo: Перемешивает строки в файле
